#!/usr/bin/python3

"""Subcommands to build something, e.g., a debian package or a cmake project."""

import json
import os

import typer
from typing_extensions import Annotated

from devcli_utils import CommandRunner, ConfigBuild, config_from_json

app = typer.Typer()


###############################################################################
# COMMAND AUTO-COMPLETIONS
###############################################################################


def complete_dpkg_config() -> list[str]:
    """Autocompletion for dpgk configuration.

    Returns:
        List of available dpgk configurations.
    """
    configs: list[str] = []

    config = config_from_json()

    if config.build is not None:
        configs = list(config.build.dpkg_configs())

    return configs


def complete_cmake_config() -> list[str]:
    """Autocompletion for cmake configuration.

    Returns:
        List of available cmake configurations.
    """
    configs: list[str] = []

    config = config_from_json()

    if config.build is not None:
        configs = list(config.build.cmake_configs())

    return configs


###############################################################################
# COMMANDS
###############################################################################


@app.command()
def config_schema():
    """Prints the configuration schema for build commands.

    The configuration has to be stored in the devcli.json,
    devcli will automatically search for that file in the current working directory.
    """
    print(json.dumps(ConfigBuild.model_json_schema(), indent=2))


@app.command()
def dpkg(
    config: Annotated[
        str,
        typer.Option(
            "--config",
            "-c",
            help="Use predefined configuration",
            autocompletion=complete_dpkg_config,
        ),
    ],
    verbose: Annotated[
        bool, typer.Option("--verbose", "-v", help="Enable verbose output")
    ] = False,
):
    """Builds a debian package.

    Building debian packages is only working with configs.
    Providing the settings via command line options is currently not supported.
    """
    runner = CommandRunner(description="Build debian package", verbose=verbose)

    if config is None:
        runner.add_error("Invalid configuration, please check devcli.json")
        return

    build_config = config_from_json().build
    if build_config is None:
        runner.add_error("Invalid build configuration, please check devcli.json")
        return

    dpkg_config = build_config.dpkg_config(config)
    if dpkg_config is None:
        runner.add_error("Invalid dpkg configuration, please check devcli.json")
        return

    verbose = dpkg_config.verbose | verbose

    if not os.path.exists(dpkg_config.build_dir):
        runner.add_error(
            "Invalid build directory ({}), please check devcli.json".format(
                dpkg_config.build_dir
            )
        )
        return

    for key, value in dpkg_config.env_vars.items():
        runner.add_env_var(key, value)

    for pre_command in dpkg_config.pre_commands:
        runner.run(pre_command)

    runner.run(
        "dpkg-deb -Zgzip --build {} {}".format(dpkg_config.build_dir, dpkg_config.name)
    )

    if not runner.ok():
        raise typer.Exit(code=1)


@app.command()
def cmake(
    config: Annotated[
        str,
        typer.Option(
            "--config",
            "-c",
            help="Use predefined configuration",
            autocompletion=complete_cmake_config,
        ),
    ],
    verbose: Annotated[
        bool, typer.Option("--verbose", "-v", help="Enable verbose output")
    ] = False,
):
    """Builds a cmake project.

    Building cmake projects is only working with configs.
    Providing the settings via command line options is currently not supported.
    """
    runner = CommandRunner(description="Build cmake project", verbose=verbose)

    # Do some basic validations
    if config is None:
        runner.add_error("Invalid configuration, please check devcli.json")
        return

    build_config = config_from_json().build
    if build_config is None:
        runner.add_error("Invalid build configuration, please check devcli.json")
        return

    cmake_config = build_config.cmake_config(config)
    if cmake_config is None:
        runner.add_error("Invalid cmake configuration, please check devcli.json")
        return

    # Set verbose flag
    verbose = cmake_config.verbose | verbose

    # Set environment variables
    for key, value in cmake_config.env_vars.items():
        runner.add_env_var(key, value)

    # Run all pre-commands
    for pre_command in cmake_config.pre_commands:
        runner.run(pre_command)

    # Prepare build directory
    runner.run(
        [
            "rm -rf {}".format(cmake_config.build_dir),
            "mkdir -p {}".format(cmake_config.build_dir),
        ]
    )

    # Prepare conan packages, if a config is given
    #   The following build will then use the conan toolchain file,
    #   its path is thus just added as definition
    if cmake_config.conan is not None:
        cmd = "conan install ."
        cmd += " \\\n --output-folder {}".format(cmake_config.build_dir)
        cmd += " \\\n --profile:host  {}".format(cmake_config.conan.profile_host)
        cmd += " \\\n --profile:build {}".format(cmake_config.conan.profile_build)
        if cmake_config.conan.build_missing:
            cmd += " \\\n --build missing"
        runner.run(cmd)

        cmake_config.definitions["CMAKE_TOOLCHAIN_FILE"] = os.path.join(
            cmake_config.build_dir, "conan_toolchain.cmake"
        )

    # Build and run cmake commands
    cmd = "cmake -B {}".format(cmake_config.build_dir)
    for key, value in cmake_config.definitions.items():
        cmd += " \\\n -D{}={}".format(key, value)
    runner.run(cmd)
    runner.run("cmake --build {} -j".format(cmake_config.build_dir))

    compile_commands = os.path.join(cmake_config.build_dir, "compile_commands.json")
    if os.path.exists(compile_commands):
        runner.run("cp {} .".format(compile_commands))

    if not runner.ok():
        raise typer.Exit(code=1)


if __name__ == "__main__":
    app()
