#!/usr/bin/python3

"""Subcommands regarding nrf52 chips, e.g., flashing."""

from importlib.resources import files

import typer
from typing_extensions import Annotated

from devcli_utils import CommandRunner

app = typer.Typer()


###############################################################################
# COMMANDS
###############################################################################


@app.command()
def reset(
    verbose: Annotated[
        bool,
        typer.Option("--verbose", "-v", help="Enable verbose output"),
    ] = False,
):
    """Resets the connected nRF52 device."""
    runner = CommandRunner(description="Reset nRF52 device", verbose=verbose)
    runner.run(
        "sudo openocd -f interface/cmsis-dap.cfg -d2 -f target/nrf52.cfg -c 'init; reset; shutdown'"
    )

    if not runner.ok():
        raise typer.Exit(code=1)


@app.command()
def eraseall(
    verbose: Annotated[
        bool,
        typer.Option("--verbose", "-v", help="Enable verbose output"),
    ] = False,
):
    """Erases the connected nRF52 device."""
    runner = CommandRunner(description="Erase nRF52 device", verbose=verbose)

    scripts = files("devcli_nrf52.scripts")
    script = scripts.joinpath("eraseall.tcl")

    runner.run(
        "sudo openocd -f interface/cmsis-dap.cfg -d2 -f target/nrf52.cfg -f {}".format(
            script
        )
    )

    if not runner.ok():
        raise typer.Exit(code=1)


@app.command()
def program(
    file: Annotated[
        str,
        typer.Option("--file", "-f", help="File to be flashed (*.hex, *.elf)"),
    ],
    verbose: Annotated[
        bool,
        typer.Option("--verbose", "-v", help="Enable verbose output"),
    ] = False,
):
    """Programs the connected nRF52 device."""
    runner = CommandRunner(description="Program nRF52 device", verbose=verbose)
    runner.run(
        "sudo openocd -f interface/cmsis-dap.cfg -d2 -f target/nrf52.cfg -c 'program {} verify reset exit'".format(
            file
        ),
        attempts=2,
    )

    if not runner.ok():
        raise typer.Exit(code=1)


@app.command()
def write_uicr(
    serial_number: Annotated[
        int,
        typer.Option("--serial-number", "-s", help="Serial number to be written"),
    ],
    feature_map: Annotated[
        int,
        typer.Option("--feature-map", "-f", help="Feature map to be written"),
    ],
    verbose: Annotated[
        bool,
        typer.Option("--verbose", "-v", help="Enable verbose output"),
    ] = False,
):
    """Write UICR registers of the connected nRF52 device.

    - 0x10001080: Serial number
    - 0x10001084: Feature map
    """
    runner = CommandRunner(description="Erase nRF52 device", verbose=verbose)

    scripts = files("devcli_nrf52.scripts")
    script = scripts.joinpath("write_uicr.tcl")

    runner.run(
        "sudo openocd -f interface/cmsis-dap.cfg -d2 -f target/nrf52.cfg -c 'set sn {}' -c 'set fm {}' -f {}".format(
            serial_number, feature_map, script
        )
    )

    if not runner.ok():
        raise typer.Exit(code=1)


if __name__ == "__main__":
    app()
