
puts "Initialize target..."
init

puts "Enable erase..."
write_memory 0x4001E504 32 {0x02}
while {[read_memory 0x4001E400 32 1] != 0x01} {
  sleep 1
}

puts "Eraseall..."
write_memory 0x4001E50C 32 {0x01}
while {[read_memory 0x4001E400 32 1] != 0x01} {
  sleep 1
}

puts "Disable erase..."
write_memory 0x4001E504 32 {0x00}
while {[read_memory 0x4001E400 32 1] != 0x01} {
  sleep 1
}

puts "Reset target..."
reset

puts "Shutdown target..."
shutdown

