
puts "Initialize target..."
init

puts "Enable erase..."
write_memory 0x4001E504 32 {0x02}
while {[read_memory 0x4001E400 32 1] != 0x01} {
  sleep 1
}

puts "Erase UICR..."
write_memory 0x4001E514 32 {0x01}
while {[read_memory 0x4001E400 32 1] != 0x01} {
  sleep 1
}

puts "Enable write..."
write_memory 0x4001E504 32 {0x01}
while {[read_memory 0x4001E400 32 1] != 0x01} {
  sleep 1
}

puts "Write UICR... (SerialNumber)"
write_memory 0x10001080 32 $sn
while {[read_memory 0x4001E400 32 1] != 0x01} {
  sleep 1
}

puts "Write UICR... (FeatureMap)"
write_memory 0x10001084 32 $fm
while {[read_memory 0x4001E400 32 1] != 0x01} {
  sleep 1
}

puts "Disable write..."
write_memory 0x4001E504 32 {0x00}
while {[read_memory 0x4001E400 32 1] != 0x01} {
  sleep 1
}

puts "Reset target..."
reset

puts "Read UICR..."
puts [read_memory 0x10001080 32 1]
puts [read_memory 0x10001084 32 1]
puts [read_memory 0x1000120C 32 1]

puts "Shutdown target..."
shutdown

