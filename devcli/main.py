#!/usr/bin/python3

"""Main entry point of the devcli application."""

import importlib.metadata
import json

import typer
from rich import print
from rich.panel import Panel
from rich.table import Table

from devcli_build import build
from devcli_crypto import crypto
from devcli_nrf52 import nrf52
from devcli_remote import remote
from devcli_utils import Config

app = typer.Typer()
app.add_typer(
    build.app, name="build", help="Helper functions for building artifacts and packages"
)
app.add_typer(remote.app, name="remote", help="Handling of remote devices")
app.add_typer(crypto.app, name="crypto", help="Cryptographic helper functions")
app.add_typer(nrf52.app, name="nrf52", help="Handling of nRF52 devices")


###############################################################################
# COMMANDS
###############################################################################


@app.command()
def about():
    """Returns information about this application."""
    metadata = importlib.metadata.metadata("py-devcli")
    table = Table(show_header=False, box=None, style="white")
    table.add_column("")
    table.add_column("")
    table.add_row("Name", metadata["Name"], style="white")
    table.add_row("Summary", metadata["Summary"], style="white")
    table.add_row("Author", metadata["Author"], style="white")
    table.add_row("Version", metadata["Version"], style="white")

    print(Panel.fit(table, title="[bold] About [/bold]", style="cyan"))


@app.command()
def config_schema():
    """Prints the configuration schema.

    The configuration has to be stored in the devcli.json,
    devcli will automatically search for that file in the current working directory.
    """
    print(json.dumps(Config.model_json_schema(), indent=2))
