## 0.4.0 (2024-12-21)

- Fixed return code handling
- Run nrf52 program task twice in case an error occured
- Added support for feature map in nRF52 UICR registers

## 0.3.1 (2024-10-19)

- Fixed verbose logging of commands

## 0.3.0 (2024-10-18)

- Fixed docstrings
- Enhanced static code checks (flake8-annotations, flake8-docstrings, mypy)

## 0.2.0 (2024-06-01)

- Support of build commands for dpkg and cmake
- Support of static configurations for remote and build commands
- Enhanced command runner

## 0.1.0 (2024-03-25)

- Support of remote commands
- Support of crypto commands
- Support of nrf52 commands
