#!/usr/bin/python3

"""Subcommands for remote handling via SSH."""

import glob
import json
import os

import paramiko
import typer
from typing_extensions import Annotated

from devcli_utils import CommandResult, CommandRunner, ConfigRemote, config_from_json

app = typer.Typer()

###############################################################################
# COMMAND AUTO-COMPLETIONS
###############################################################################


def complete_upload_file() -> list[str]:
    """Autocompletion for upload file.

    Returns:
        List of available upload files.
    """
    files: list[str] = []

    path = os.path.abspath(os.getcwd())
    files.extend(glob.glob(r"{}/*.deb".format(path)))

    return files


def complete_upload_config() -> list[str]:
    """Autocompletion for upload config.

    Returns:
        List of available upload configs.
    """
    configs: list[str] = []

    config = config_from_json()

    if config.remote is not None:
        configs = list(config.remote.upload_configs())

    return configs


###############################################################################
# COMMANDS
###############################################################################


@app.command()
def config_schema():
    """Prints the configuration schema for remote commands.

    The configuration has to be stored in the devcli.json,
    devcli will automatically search for that file in the current working directory.
    """
    print(json.dumps(ConfigRemote.model_json_schema(), indent=2))


@app.command()
def configure_gateway(
    nic_public: Annotated[
        str,
        typer.Option(
            "--nic-public", "-p", help="NIC that is connected to the Internet"
        ),
    ] = "wlp113s0",
    nic_remote: Annotated[
        str,
        typer.Option(
            "--nic-remote", "-r", help="NIC thats is connected t o the remote device"
        ),
    ] = "enp0s31f6",
    remote_ipv4: Annotated[
        str,
        typer.Option("--remote-ipv4", "-i", help="IPv4 network of the remote device"),
    ] = "192.168.42.0/24",
    verbose: Annotated[
        bool,
        typer.Option("--verbose", "-v", help="Enable verbose output"),
    ] = False,
):
    """Configures the iptables to provide Internet access to the remote device."""
    runner = CommandRunner(description="Configure iptables", verbose=verbose)
    runner.run(
        [
            "sudo iptables -A FORWARD -o {} -i {} -s {} -m conntrack --ctstate NEW -j ACCEPT".format(
                nic_public, nic_remote, remote_ipv4
            ),
            "sudo iptables -A FORWARD -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT",
            "sudo iptables -t nat -A POSTROUTING -o wlp113s0 -j MASQUERADE",
        ]
    )

    if not runner.ok():
        raise typer.Exit(code=1)


@app.command()
def upload(
    file: Annotated[
        str,
        typer.Option(
            "--file",
            "-f",
            help="File to be uploaded",
            autocompletion=complete_upload_file,
        ),
    ] = "",
    install: Annotated[
        bool,
        typer.Option("--install", "-i", help="Install file on remote after upload"),
    ] = False,
    remote_path: Annotated[
        str,
        typer.Option("--remote-path", help="File destination"),
    ] = "/tmp",
    remote_ipv4: Annotated[
        str,
        typer.Option("--remote-ipv4", help="IPv4 address of the remote device"),
    ] = "192.168.42.1",
    remote_port: Annotated[
        int,
        typer.Option("--remote-port", help="SSH port of the remote device"),
    ] = 22,
    remote_user: Annotated[
        str,
        typer.Option("--remote-user", help="Username for the remote device"),
    ] = "pi",
    remote_pw: Annotated[
        str,
        typer.Option("--remote-pw", help="Password for the remote device"),
    ] = "pi",
    verbose: Annotated[
        bool, typer.Option("--verbose", "-v", help="Enable verbose output")
    ] = False,
    config: Annotated[
        str,
        typer.Option(
            "--config",
            "-c",
            help="Use predefined configuration",
            autocompletion=complete_upload_config,
        ),
    ] = "",
):
    """Uploads a file to a remote device.

    If a config is defined, this will overwrite all other given values.
    """
    runner = CommandRunner(description="Upload file to remote", verbose=verbose)

    if config is not None:
        remote_config = config_from_json().remote
        if remote_config is not None:
            upload_config = remote_config.upload_config(config)
            if upload_config is not None:
                file = upload_config.file
                install = upload_config.install
                remote_path = upload_config.remote_path
                remote_ipv4 = upload_config.remote_ipv4
                remote_port = upload_config.remote_port
                remote_user = upload_config.remote_user
                remote_pw = upload_config.remote_pw
                verbose = upload_config.verbose

    if os.path.isdir(remote_path):
        remote_path = os.path.join(remote_path, os.path.basename(file))

    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(
        remote_ipv4,
        port=remote_port,
        username=remote_user,
        password=remote_pw,
        timeout=5,
    )

    sftp = ssh.open_sftp()
    try:
        sftp.mkdir(os.path.dirname(remote_path))
    except Exception:
        pass
    sftp.put(file, remote_path)
    sftp.close()
    runner.add_result(
        CommandResult(
            code=0,
            stdin="scp -P {} {} {}@{}:{}".format(
                remote_port, file, remote_user, remote_ipv4, remote_path
            ),
        )
    )

    if install:
        cmd = None

        file_ext = os.path.splitext(file)[1]
        if file_ext == ".deb":
            cmd = "sudo dpkg -i {}".format(remote_path)

        if cmd is not None:
            _, stdout, stderr = ssh.exec_command(cmd, timeout=30)
            runner.add_result(
                CommandResult(
                    code=stdout.channel.recv_exit_status(),
                    stdin="(remote) {}".format(cmd),
                    stdout=stdout.read().decode("utf-8"),
                    stderr=stderr.read().decode("utf-8"),
                )
            )

    if not runner.ok():
        raise typer.Exit(code=1)


if __name__ == "__main__":
    app()
