"""Configuration of the devcli application."""

import json
import os
from typing import List

from pydantic import BaseModel


class ConfigBuildDpkg(BaseModel):
    """Dpkg build configuration.

    Attributes:
        id:
            Unique ID of this configuration.
        name:
            Name of the resulting dpgk package.
        build_dir:
            Build directory.
        env_vars:
            List of environment variables.
        pre_commands:
            List of commands that shall be executed in advance.
        verbose:
            Flag to enable/disable verbose logging.
    """

    id: str
    name: str
    build_dir: str = "./debpackage"
    env_vars: dict = {}
    pre_commands: List[str] = []
    verbose: bool = False


class ConfigBuildCMakeConan(BaseModel):
    """Conan configuration.

    Attributes:
        profile_host:
            Host profile to be used.
        profile_build:
            Build profile to be used.
        build_missing:
            Enable/disable the --build-missing flag
    """

    profile_host: str
    profile_build: str
    build_missing: bool = True


class ConfigBuildCMake(BaseModel):
    """Cmake build configuration.

    Attributes:
        id:
            Unique ID of this configuration.
        build_dir:
            Build directory.
        env_vars:
            List of environment variables.
        pre_commands:
            List of commands that shall be executed in advance.
        definitions:
            Definitions to be set.
        conan:
            Conan configuration.
        verbose:
            Flag to enable/disable verbose logging.
    """

    id: str
    build_dir: str = "./build"
    env_vars: dict = {}
    pre_commands: List[str] = []
    definitions: dict = {}
    conan: ConfigBuildCMakeConan | None = None
    verbose: bool = False


class ConfigBuild(BaseModel):
    """Build configurations.

    Attributes:
        dpkg:
            List of dpkg configurations.
        cmake:
            List of cmake configurations.
    """

    dpkg: List[ConfigBuildDpkg] = []
    cmake: List[ConfigBuildCMake] = []

    def dpkg_configs(self):
        """Returns all dpkg configurations."""
        for config in self.dpkg:
            yield config.id

    def dpkg_config(self, id: str):
        """Returns dpkg configuration with specified ID."""
        for config in self.dpkg:
            if config.id == id:
                return config

    def cmake_configs(self):
        """Returns all cmake configurations."""
        for config in self.cmake:
            yield config.id

    def cmake_config(self, id: str):
        """Returns cmake configuration with specified ID."""
        for config in self.cmake:
            if config.id == id:
                return config


class ConfigRemoteUpload(BaseModel):
    """Remote upload configuration.

    Attributes:
        id:
            Unique ID of this configuration.
        file:
            Path of the file to be uploaded.
        install:
            Flag to install the file after upload.
        remote_path:
            Destination path on the remote device.
        remote_ipv4:
            IPv4 address of the remote device.
        remote_port:
            SSH port of the remote device.
        remot_user:
            User of the remote device.
        remote_pw:
            Password of the remote device user.
        verbose:
            Flag to enable/disable verbose logging.
    """

    id: str
    file: str
    install: bool = False
    remote_path: str
    remote_ipv4: str
    remote_port: int = 22
    remote_user: str
    remote_pw: str
    verbose: bool = False


class ConfigRemote(BaseModel):
    """Remote configurations.

    Attributes:
        upload:
            List of upload configurations.
    """

    upload: List[ConfigRemoteUpload] = []

    def upload_configs(self):
        """Returns all upload configurations."""
        for config in self.upload:
            yield config.id

    def upload_config(self, id: str):
        """Returns upload configuration with specified ID."""
        for config in self.upload:
            if config.id == id:
                return config


class Config(BaseModel):
    """Configuration of the devcli applicatoin.

    Attributes:
        build:
            Build related configuration.
        remote:
            Remote related configuration.
    """

    build: ConfigBuild | None = None
    remote: ConfigRemote | None = None


def config_from_json(file: str = "") -> Config:
    """Reads the config from a json file.

    Args:
        file:
            Path of the json file.

    Returns:
        Configuration for devcli.
    """
    if file == "":
        file = os.path.join(os.getcwd(), "devcli.json")

    if os.path.exists(file):
        with open(file) as f:
            data = json.load(f)
            return Config.model_validate(data)

    return Config()
