"""Helper functions regarding system commands."""

import datetime
import os
import subprocess
import sys

from pydantic import BaseModel
from rich import print
from rich.console import Group
from rich.panel import Panel
from rich.status import Status
from rich.table import Table


class CommandResult(BaseModel):
    """Result of a command.

    Attributes:
        code:
            Return code of the command.
        stdin:
            Input of the command.
        stdout:
            Output of the command.
        stderr:
            Error output of the command.
    """

    code: int
    stdin: str = ""
    stdout: str = ""
    stderr: str = ""

    def ok(self) -> bool:
        """Checks if the command was successful.

        Returns:
            True if successful, false in case of any error.
        """
        return self.code == 0


class CommandRunner:
    """Helper class to run commands."""

    def __init__(self, description: str = "", verbose: bool = False):
        """Constructor.

        Args:
            description:
                Description of the command.
            verbose:
                Flag to enable/disable the verbose logging.
        """
        self.__description: str = description
        self.__verbose: bool = verbose
        self.__ts_start = datetime.datetime.now()
        self.__env_vars: dict = {}
        self.__results: list[CommandResult] = []

    def __del__(self):
        """Destructor."""
        for key, _ in self.__env_vars.items():
            os.environ.pop(key)
        self.__ts_end = datetime.datetime.now()
        self.__print()

    def add_env_var(self, key: str, value: str) -> None:
        """Adds an environment variable.

        Args:
            key:
                Key of the environment variable.
            value:
                Value of the environment variable.

        Returns:
            None
        """
        self.__env_vars[key] = value
        os.environ[key] = value

    def run(self, commands: str | list[str], attempts: int = 1) -> None:
        """Runs one or more commands.

        Args:
            command:
                Single command or list of commands to be run.
            attempts:
                Number of attempts in case an error occured.

        Returns:
            None
        """
        cmds = [commands] if isinstance(commands, str) else commands
        for cmd in cmds:
            self.__results.append(self.__run_command(cmd, attempts))

    def add_result(self, result: CommandResult) -> None:
        """Adds a command result.

        Args:
            result:
                Result to be added.

        Returns:
            None
        """
        self.__results.append(result)

    def add_error(self, message: str) -> None:
        """Adds an error.

        Args:
            message:
                Error message.

        Returns:
            None
        """
        self.__results.append(CommandResult(code=-1, stderr=message))

    def ok(self) -> bool:
        """Return status of all run commands.

        Returns:
            True in case no error has occured.
        """
        return not self.__any_error(self.__results)

    def __print(self) -> None:
        """Prints a summary of all commands.

        Returns:
            None
        """
        table_env_vars = Table(show_header=False, box=None, style="white")
        table_env_vars.add_column("", style="white")
        table_env_vars.add_column("", style="white")
        for key, value in self.__env_vars.items():
            table_env_vars.add_row(key, value)

        table_results = Table(show_header=False, box=None, style="white")
        table_results.add_column("", style="white")
        table_results.add_column("", style="white")

        for result in self.__results:
            c1 = str(result.code)
            c2 = result.stdin
            # if self.__verbose:
            #     c2 += "\n[grey54]" + result.stdout + "[/grey54]"
            #     c2 += "\n[grey54]" + result.stderr + "[/grey54]"
            table_results.add_row(c1, c2)
            table_results.add_row("", "")

        table_results.add_row("RUNTIME", str(self.__ts_end - self.__ts_start))

        if datetime.datetime.now().month == 12:
            table_results.add_row("", "")
            table_results.add_row(
                "",
                ":santa: [bold][white]HO[/white][red]HO[/red][white]HO[/white][/bold] :santa:",
            )

        # Create and print panel
        if len(self.__env_vars) > 0:
            group = Group(
                Panel(self.__description, style="bold white"),
                Panel(table_env_vars, style="white"),
                Panel(table_results, style="white"),
            )
        else:
            group = Group(
                Panel(self.__description, style="bold white"),
                Panel(table_results, style="white"),
            )
        print(
            Panel.fit(
                group,
                title="[bold] Result [/bold]",
                style=("green" if not self.__any_error(self.__results) else "red"),
            )
        )

    def __run_command(self, command: str, attempts: int = 1) -> CommandResult:
        """Runs a single command.

        Args:
            command:
                Command to be run.
            attempts:
                Number of attempts in case an error occured.

        Returns:
            Result of the command.
        """
        spinner = "christmas" if datetime.datetime.now().month == 12 else "dots"

        with Status(
            "[bold cyan] {}".format(command),
            spinner=spinner,
            spinner_style="bold cyan",
        ) as _:
            for _ in range(attempts):
                p = subprocess.Popen(
                    command,
                    stdout=subprocess.PIPE,
                    stderr=subprocess.STDOUT,
                    shell=True,
                    encoding="utf-8",
                    bufsize=1,
                )

                stdout: str = ""
                stderr: str = ""
                while p.poll() is None:
                    if p.stdout is not None:
                        line = p.stdout.readline()
                        stdout += line
                        if self.__verbose:
                            sys.stdout.write(line)

                    if p.stderr is not None:
                        line = p.stderr.readline()
                        stderr += line
                        if self.__verbose:
                            sys.stdout.write(line)
                sys.stdout.flush()

                result = CommandResult(
                    code=p.returncode, stdin=command, stdout=stdout, stderr=stderr
                )

                if result.code == 0:
                    break

        return result

    def __any_error(self, results: list[CommandResult]) -> bool:
        """Checks if any error occured.

        Args:
            results:
                List of results to be checked.

        Returns:
            True if an error occured, false otherwise.
        """
        for result in results:
            if result.code != 0:
                return True

        return False
