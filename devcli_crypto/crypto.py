#!/usr/bin/python3

"""Subcommands encryption and decryption."""

import os

import typer
from cryptography.fernet import Fernet
from typing_extensions import Annotated

app = typer.Typer()


###############################################################################
# COMMANDS
###############################################################################


@app.command()
def encrypt(
    file_in: Annotated[
        str,
        typer.Option("--file-in", "-i", help="File to be encrypted"),
    ],
    file_out: Annotated[
        str,
        typer.Option("--file-out", "-o", help="Encrypted file"),
    ],
    key: Annotated[
        str,
        typer.Option("--key", "-k", help="Key to be used"),
    ] = "vpN3j1uChhZrqHh_BBzLLhZD51UYl4MNC7HB8JTi_YA=",
    generate_key: Annotated[
        bool,
        typer.Option("--generate-key", "-g", help="Generate new key"),
    ] = False,
):
    """Encrypts a given file."""
    if generate_key:
        key = Fernet.generate_key().decode("utf-8")
        print("Generated key: {}".format(key))

    fernet = Fernet(key)

    with open(file_in, "rb") as file:
        original = file.read()

    encrypted = fernet.encrypt(original)

    os.makedirs(os.path.dirname(file_out), exist_ok=True)
    with open(file_out, "wb") as file:
        file.write(encrypted)


@app.command()
def decrypt(
    file_in: Annotated[
        str,
        typer.Option("--file-in", "-i", help="File to be decrypted"),
    ],
    file_out: Annotated[
        str,
        typer.Option("--file-out", "-o", help="Decrypted file"),
    ],
    key: Annotated[
        str,
        typer.Option("--key", "-k", help="Key to be used"),
    ] = "vpN3j1uChhZrqHh_BBzLLhZD51UYl4MNC7HB8JTi_YA=",
):
    """Decrypts a given file."""
    fernet = Fernet(key)

    with open(file_in, "rb") as file:
        encrypted = file.read()

    original = fernet.decrypt(encrypted)

    os.makedirs(os.path.dirname(file_out), exist_ok=True)
    with open(file_out, "wb") as file:
        file.write(original)


if __name__ == "__main__":
    app()
