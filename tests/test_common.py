"""Test module for the common commands."""

import subprocess


def test_about():
    """Test of the about command."""
    result = subprocess.run(
        "devcli about", stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True
    )

    assert 0 == result.returncode
    assert 0 == len(result.stderr)
