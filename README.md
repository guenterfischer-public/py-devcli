# py-devcli

Command line tool for common developer tasks.

![pipeline status](https://gitlab.com/guenterfischer-public/py-devcli/badges/main/pipeline.svg)

## Developer Guide

### Installation

```bash
# Open the virtualenv
poetry shell

# Build and install the tool
poetry install

# Check if the tool has been installed
which devcli
```

### Testing

```bash
# Run all tests that matches ./tests/test_*.py
poetry run pytest
```

### Deployment

```bash
# Create sdist and wheel
poetry build
```

## Installation

```bash
# Install previously built package on any machine with...
sudo pip install ./dist/devcli-0.1.0-py3-none-any.whl

# Install from package registry
sudo pip install py-devcli --index-url https://gitlab.com/api/v4/projects/56134089/packages/pypi/simple
```

Once the tool is installed, autocompletion can be enabled as follows:

```bash
devcli --install-completion

. ~/.bashrc
```

## Usage

### Common

Show information about the tool itself:

```bash
devcli about
```

The tool supports to read settings from a configuration file. \
The name of the file has to be devcli.json and it has to be located in the current working directory. \
The schema can be printed with this command:

```bash
devcli config-schema
```

### Build

This module provides functions for building artifacts and packages, e.g., debian packages.

```bash
devcli build --help
```

Creating a debian package based on a predefined config, which is located in the devcli.json file. \
The json file must be in the directory where devcli is executed.

```json
{
  "build": {
    "dpkg": [
      {
        "id": "deb",
        "name": "my-app.deb",
        "env_vars": {
          "DEB_PATH": "./debpackage/usr/local/sbin"
        },
        "pre_commands": [
          "rm -rf $DEB_PATH",
          "mkdir -p $DEB_PATH",
          "cp -r ./sources/. $DEB_PATH"
        ]
      }
    ]
  }
}
```

```bash
# Creating a debian package according to the configuration in devcli.json
#   The option 'config' supports autocompletion
devcli build dpkg --config deb
```

Building a cmake project based on a predefined config, which is located in the devcli.json file. \
The json file must be in the directory where devcli is executed.

```json
{
  "build": {
    "cmake": [
      {
        "id": "release",
        "definitions": {
          "BOARD": "my_nrf52833",
          "CMAKE_BUILD_TYPE": "Release"
        },
        "env_vars": {
          "ZEPHYR_BOARDS": "/home/docker/zephyrproject/zephyr/boards/arm"
        },
        "pre_commands": [
          "rm -rf $ZEPHYR_BOARDS/my_nrf52833",
          "cp -r ../zephyr-boards/my_nrf52833 $ZEPHYR_BOARDS/my_nrf52833"
        ]
      }
    ]
  }
}
```

```bash
# Building a cmake project according to the configuration in devcli.json
#   The option 'config' supports autocompletion
devcli build cmake --config release
```

It is also possible to include conan in the build process. \
The configuration will then look like this:

```json
{
  "build": {
    "cmake": [
      {
        "id": "release",
        "definitions": {
          "CMAKE_BUILD_TYPE": "Release"
        },
        "build_dir": "./build/gcc11__x86_64-pc-linux-elf__release",
        "conan": {
          "profile_host": "./tools/conan/gcc11__x86_64-pc-linux-elf__release",
          "profile_build": "./tools/conan/gcc11__x86_64-pc-linux-elf__release",
          "build_missing": true
        }
      }
    ]
  }
}
```

### Remote

This module provides functions for working with remote devices, such as a Raspberry Pi.

```bash
devcli remote --help
```

Configure a gateway to provide Internet access to a remote device:

```bash
# Show available options
devcli remote configure-gateway --help

# Run with default options
devcli remote configure-gateway
```

Upload a file to a remote device and optionally install it:

```bash
# Show available options
devcli remote upload --help

# Upload and install a debian package
#   The option 'file' supports autocompletion
devcli remote upload --file /tmp/my-package.deb --install --verbose
```

The upload can also be done based on a predefined config, which is located in the devcli.json file. \
The json file must be in the directory where devcli is executed.

```json
{
  "remote": {
    "upload": [
      {
        "id": "rpi",
        "file": "./my-app.deb",
        "install": true,
        "remote_path": "/tmp",
        "remote_ipv4": "192.168.42.1",
        "remote_port": 22,
        "remote_user": "pi",
        "remote_pw": "pi",
        "verbose": true
      }
    ]
  }
}
```

```bash
# Upload according to the configuration in devcli.json
#   The option 'config' supports autocompletion
devcli remote upload --config rpi
```

### Crypto

This module provides functions for encryption and decryption.

```bash
devcli crypto --help
```

Encryption and decryption of a file:

```bash
devcli crypto encrypt --file-in /tmp/crypto.txt --file-out /tmp/crypto-enc.txt
devcli crypto decrypt --file-in /tmp/crypto-enc.txt --file-out /tmp/crypto-dec.txt
```

### nRF52

This module provides functions for working with nRF52 devices.

```bash
devcli nrf52 --help
```

Programming a device and setting its serial number can be done as follows. \
The UICR register 0x10001080 is used for the serial number. To use this register is defined on myself and is not given by the vendor.

```bash
devcli nrf52 reset --verbose
devcli nrf52 eraseall --verbose
devcli nrf52 program --file /tmp/app.hex --verbose
devcli nrf52 write-uicr --serial-number 42 --verbose
```
